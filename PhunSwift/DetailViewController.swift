//
//  DetailViewController.swift
//  PhunSwift
//
//  Created by Molly Rand on 2/29/16.
//  Copyright © 2016 Phunware. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var detailDescriptionLabel: UILabel!
    @IBOutlet weak var detailImageView: UIImageView!
    @IBOutlet weak var detailDateView: UILabel!
    @IBOutlet weak var detailTitle: UILabel!
    @IBOutlet weak var contentHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var imageGradient : CAGradientLayer?

    var chosenEvent: PhunEvent? 

    func configureView() {
        if let event = self.chosenEvent {
            let style = NSMutableParagraphStyle()
            style.lineSpacing = 20
            let attributes = [NSParagraphStyleAttributeName : style]
            self.detailDescriptionLabel.attributedText = NSAttributedString(string: event.eventDescription, attributes: attributes)
            if let URL = NSURL(string: event.imageUrl) {
                self.detailImageView.kf_setImageWithURL(URL, placeholderImage: nil)
            }
            self.detailTitle.text = event.title
            self.detailDateView.text = event.getDateString()
            contentHeight.constant = max(UIScreen.mainScreen().bounds.height, UIScreen.mainScreen().bounds.width) * 2
            scrollView.delegate = self
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        let imgFrame = detailImageView.bounds
        imageGradient = CAGradientLayer()
        imageGradient!.frame = imgFrame
        let translucentColor = UIColor(white: 1, alpha: 0.6)
        imageGradient!.colors = [UIColor.clearColor().CGColor, translucentColor.CGColor, UIColor.whiteColor().CGColor]
        imageGradient!.locations = [0.0, 0.6, 0.9]
        detailImageView.layer.insertSublayer(imageGradient!, atIndex: 0)
        super.viewDidAppear(animated)
        
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        if let gradient = imageGradient {
            gradient.removeFromSuperlayer()
            let imgFrame = CGRectMake(0, 0, size.width, size.width * (46/75))
            gradient.frame = imgFrame
            let translucentColor = UIColor(white: 1, alpha: 0.6)
            gradient.colors = [UIColor.clearColor().CGColor, translucentColor.CGColor, UIColor.whiteColor().CGColor]
            gradient.locations = [0.0, 0.6, 0.9]
            detailImageView.layer.insertSublayer(gradient, atIndex: 0)
        }
    }
    
    override func willRotateToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
        if let gradient = imageGradient {
            gradient.removeFromSuperlayer()
            let imgFrame = CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, UIScreen.mainScreen().bounds.width * (46/75))
            gradient.frame = imgFrame
            let translucentColor = UIColor(white: 1, alpha: 0.6)
            gradient.colors = [UIColor.clearColor().CGColor, translucentColor.CGColor, UIColor.whiteColor().CGColor]
            gradient.locations = [0.0, 0.6, 0.9]
            detailImageView.layer.insertSublayer(gradient, atIndex: 0)
        }
    }
    
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    @IBAction func action(sender: AnyObject) {
        if let event = self.chosenEvent {
            var items = [event.title, event.getDateString(), event.location1, event.location2, event.eventDescription]
            if let phoneNumber = event.phone {
                items.append(phoneNumber)
            }
            let activityViewController = UIActivityViewController(activityItems:items, applicationActivities: nil)
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Phone {
                self.presentViewController(activityViewController, animated: true, completion: nil)
            }
            else {
                let popup: UIPopoverController = UIPopoverController(contentViewController: activityViewController)
                popup.presentPopoverFromRect(CGRectMake(self.view.frame.size.width / 2, self.view.frame.size.height / 4, 0, 0), inView: self.view, permittedArrowDirections: UIPopoverArrowDirection.Any, animated: true)
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
    }
}

extension DetailViewController : UIScrollViewDelegate {
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if (scrollView.contentOffset.y >= detailImageView.bounds.height) {
            self.title = self.chosenEvent?.title
        } else {
            self.title = ""
        }
    }
}


