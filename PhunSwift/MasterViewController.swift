//
//  MasterViewController.swift
//  PhunSwift
//
//  Created by Molly Rand on 2/29/16.
//  Copyright © 2016 Phunware. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher
import CoreSpotlight
import MobileCoreServices

class PhunEventCell: UICollectionViewCell {
    @IBOutlet var backgroundImage : UIImageView?
    @IBOutlet var titleLabel : UILabel?
    @IBOutlet var dateLabel : UILabel?
    @IBOutlet var locLabel : UILabel?
    @IBOutlet var detailLabel : UILabel?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func loadItem(event: PhunEvent) {
        if let title = titleLabel {
            title.text = event.title
        }
        if let location = locLabel {
            location.text = event.location1
        }
        if let description = detailLabel {
            let style = NSMutableParagraphStyle()
            style.lineSpacing = 20
            let attributes = [NSParagraphStyleAttributeName : style]
            description.attributedText = NSAttributedString(string: event.eventDescription, attributes: attributes)
        }
        if let date = dateLabel {
            date.text = event.getDateString()
        }
        if let background = backgroundImage {
            if let URL = NSURL(string: event.imageUrl) {
                background.kf_setImageWithURL(URL, placeholderImage: nil)
            }
            background.alpha = 0.6
        }
    }
    
}

class MasterViewController: UICollectionViewController {

    var detailViewController: DetailViewController? = nil
    var objects = [PhunEvent]()
    let sourceUrl = "https://raw.githubusercontent.com/phunware/services-interview-resources/master/feed.json"


    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "PhunEventCell", bundle: nil)
        collectionView?.registerNib(nib, forCellWithReuseIdentifier: "PhunCell")
        Alamofire.request(.GET, sourceUrl)
        .responseJSON() { (JSON) in
            do {
                let jsonarray : NSArray = try NSJSONSerialization.JSONObjectWithData(JSON.data!, options: NSJSONReadingOptions.MutableContainers) as! NSArray
                for dict in jsonarray {
                    let event = PhunEvent(jsonDict: dict as! NSDictionary)
                    self.objects.append(event)
                }
                if #available(iOS 9.0, *) {
                    //extra: spotlight search
                    self.setupSearchableContent(self.objects)
                } 
            } catch _ {

            }
            self.collectionView?.reloadData()
        }
    }


    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "PHUN APP"
        self.navigationController?.navigationBar.setBackgroundImage(nil, forBarMetrics: UIBarMetrics.Default)
        self.navigationController?.navigationBar.shadowImage = nil
    }

    // MARK: - Segues

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showDetail" {
            if let indexPath = self.collectionView?.indexPathsForSelectedItems() {
                let object = objects[indexPath[0].row]
                let controller = segue.destinationViewController as! DetailViewController
                controller.chosenEvent = object
                self.navigationItem.title = "";
                self.navigationController?.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
                self.navigationController?.navigationBar.shadowImage = UIImage()
            }
        }
    }
    
    // MARK: - Collection View
    
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell : PhunEventCell = collectionView.dequeueReusableCellWithReuseIdentifier("PhunCell", forIndexPath: indexPath)
            as! PhunEventCell
        
        let event = objects[indexPath.row]
        cell.loadItem(event)
        return cell
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return objects.count
    }
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        performSegueWithIdentifier("showDetail", sender: self)
    }
    
    //MARK: - Rotation
    
    override func willRotateToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
        self.collectionView?.invalidateIntrinsicContentSize()
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        self.collectionView?.reloadData()
    }
    
    //MARK: - Search
    
    @available(iOS 9.0, *)
    func setupSearchableContent(contents : [PhunEvent]) {
        var searchableItems = [CSSearchableItem]()
        for i in 0 ... (contents.count - 1) {
            let event = contents[i] 
            let searchableItemAttributeSet = CSSearchableItemAttributeSet(itemContentType: kUTTypeText as String)
            searchableItemAttributeSet.title = event.title
            searchableItemAttributeSet.thumbnailURL = NSURL(string: event.imageUrl)
            searchableItemAttributeSet.contentDescription = event.eventDescription
            var keywords = [String]()
            keywords.append(event.location1)
            keywords.append(event.location2)
            for word in event.title.componentsSeparatedByString(" ") {
                keywords.append(word)
            }
            searchableItemAttributeSet.keywords = keywords
            let searchableItem = CSSearchableItem(uniqueIdentifier: "com.phunware.swiftTest.\(i)", domainIdentifier: "starwars", attributeSet: searchableItemAttributeSet)
            searchableItems.append(searchableItem)
        }
        CSSearchableIndex.defaultSearchableIndex().indexSearchableItems(searchableItems) { (error) -> Void in
            if error != nil {
                print(error?.localizedDescription)
            }
        }
    }

}

extension MasterViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            let screenSize: CGRect = UIScreen.mainScreen().bounds
            switch UIDevice.currentDevice().userInterfaceIdiom {
            case .Pad :
                return CGSizeMake((screenSize.width / 2), screenSize.height / 3)
            default:
                return CGSizeMake(screenSize.width, screenSize.height / 3)
            }
    }
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
}

