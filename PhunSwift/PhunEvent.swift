//
//  PhunEvent.swift
//  PhunSwift
//
//  Created by Molly Rand on 2/29/16.
//  Copyright © 2016 Phunware. All rights reserved.
//

import Foundation

class PhunEvent {
    var eventDescription : String
    var title : String
    var imageUrl : String
    var phone : String?
    var location1 : String
    var location2: String
    var timestamp : String
    
    init(jsonDict : NSDictionary) {
        self.eventDescription = jsonDict["description"] as! String
        self.title = jsonDict["title"] as! String
        self.imageUrl = jsonDict["image"] as! String
        if (jsonDict.objectForKey("phone") != nil) {
            self.phone = jsonDict["phone"] as? String
        }
        self.location1 = jsonDict["locationline1"] as! String
        self.location2 = jsonDict["locationline2"] as! String
        self.timestamp = jsonDict["timestamp"] as! String
        

    }
    
    func getDateString() ->String {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyy-MM-dd'T'HH:mm:ss.SSSZ"
        if let date = dateFormatter.dateFromString(timestamp) {
            dateFormatter.dateFormat = "MMMM d, yyyy 'at' h:mma"
            return dateFormatter.stringFromDate(date)
        }
        return ""
    }
}